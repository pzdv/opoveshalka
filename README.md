using System;
using System.Collections.Generic;
using System.Threading;

namespace Event_notification
{
	public class Event
	{
		public string Name;
		public DateTime dateTime;
		public string Venue;
		public Event() { }
		public Event(string N, DateTime dt, string V) { Name = N; dateTime = dt; Venue = V; }
	}

	class Program
	{
		public static void MainMenu()
		{
			Console.WriteLine("Для просмотра списка всех событий нажмите клавишу \"L\"");
			Console.WriteLine("Для создания нового события нажмите клавишу \"N\"");
			Console.WriteLine("Для удаления события нажмите клавишу \"D\"");
		}
		
		public static void printAllEvent(List<Event> Events)
		{
			Console.WriteLine();
			if(Events.Count == 0 )
				Console.WriteLine("На ближайшее время нет запланированных событий");
			else
			{
				for (int i = 0; i < Events.Count; i++)
					Console.WriteLine($"{i + 1}) " + "{0} {1} в {2}. Место проведения {3}", Events[i].dateTime.ToString("dd.MM.yyyy"), Events[i].Name, Events[i].dateTime.ToString("HH:mm"), Events[i].Venue);
			}	
		}
		public static void printNewEvent(List<Event> Events)
		{
			Console.WriteLine("Добавлено событие: {0} {1} в {2}. Место проведения {3}", Events[^1].dateTime.ToString("dd.MM.yyyy"), Events[^1].Name, Events[^1].dateTime.ToString("HH:mm"), Events[^1].Venue);
			Console.WriteLine();
		}
		public static void addNewEvent(List<Event> Events)
		{
			Console.WriteLine();
			Console.Write("Введите название события: ");
			var name = Console.ReadLine();
			Console.Write("Введите дату и время в формате \"dd.MM.yyyy HH:mm\":");
			var input = Console.ReadLine();
			DateTime dateTime;
			DateTime.TryParseExact(input, "dd.MM.yyyy HH:mm", null, System.Globalization.DateTimeStyles.None, out dateTime);
			Console.Write("Введите место проведения: ");
			var venue = Console.ReadLine();

			Events.Add(new Event(name, dateTime, venue));
			Console.WriteLine();
			printNewEvent(Events);
			Console.WriteLine();	
		}

		public static void deleteEvent(List<Event> Events)
		{
			printAllEvent(Events);
			Console.Write("Введите номер события, которое вы хотите удалить: ");
			Events.RemoveAt(int.Parse(Console.ReadLine()) - 1);
			Console.WriteLine("Событие было удалено!");
		}

		public static void Menu(List<Event> Events)
		{
			while (true)
			{
				Events.Sort((x, y) => x.dateTime.CompareTo(y.dateTime));
				MainMenu();
				var pressedKey = Console.ReadKey();

				switch (pressedKey.Key)
				{
					case ConsoleKey.L:
						{
							Console.WriteLine("\nПредстоящие события:");
							printAllEvent(Events);
							break;
						}
					case ConsoleKey.N:
						{
							addNewEvent(Events);
							break;
						}
					case ConsoleKey.D:
						{
							deleteEvent(Events);
							break;
						}
				}
			}
		}
		public static void Alert(List<Event> Events)
		{
			while(true)
			{

				if(DateTime.Now == Events[0].dateTime)
				{
					Console.WriteLine("Началось {0} в {1 }", Events[0].Name, Events[0].Venue);
					Events.RemoveAt(0);
				}
			}
		}
		static void Main(string[] args)
		{
			List<Event> Events = new List<Event>();
			Event ivent = new Event();
			ivent.Name = "собрание";
			ivent.Venue = "discord";
			ivent.dateTime = new DateTime(2020, 8, 15, 16, 30, 00);
			Events.Add(ivent);
			Event ivent1 = new Event();
			ivent1.Name = "soveshanie";
			ivent1.Venue = "kobinet312";
			ivent1.dateTime = new DateTime(2020, 8, 12, 8, 00, 00);
			Events.Add(ivent1);

			new Thread(() => Menu(Events)).Start();
			new Thread(() => Alert(Events)).Start();
		}
	}
}
